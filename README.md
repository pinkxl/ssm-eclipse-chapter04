# ssm-eclipse-chapter04

#### 介绍
第4章 Spring JDBC

#### 软件架构
基于Spring + JUnit + MySQL5


#### 运行说明

1.  直接运行单元测试用例
2.  根据需要修改配置文件

![输入图片说明](https://images.gitee.com/uploads/images/2021/0311/212810_0a26b288_382074.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

